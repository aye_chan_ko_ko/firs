import React from 'react'
import './homePageStyle.css'
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link,
    useHistory,
    useLocation,
    useParams
  } from "react-router-dom";

class HomePage extends React.Component{
    constructor(props)
    {
        super(props);
        this.state={
            display:"none",
            search:'',
            searchOpen:"block",
            searchClose:"none",
            DonerName:[
                {id:1,name:"Aye Chan",value:"PPE + Mask",price:180000,imgName:require('../src/person.jpg')},{id:2,name:"Lin Lin",value:"PPE + Mask",price:180000,},
                {id:3,name:"Mya Mya",value:"PPE + Mask",price:180000,},{id:4,name:"Tun Tun",value:"PPE + Mask",price:180000,},
                {id:5,name:"Su Nandar",value:"PPE + Mask",price:180000,},{id:6,name:"Maw Maw",value:"PPE + Mask",price:180000,},
                {id:7,name:"Khaing Ngwe",value:"PPE + Mask",price:180000,},{id:8,name:"Maung Maung",value:"PPE + Mask",price:180000,},
                {id:9,name:"Aye Aung",value:"PPE + Mask",price:180000,},{id:10,name:"Aye Aye",value:"PPE + Mask",price:180000,},
                {id:11,name:"Aung Phyo Wai",value:"PPE + Mask",price:180000,},{id:12,name:"Aye Chan Thu",value:"PPE + Mask",price:180000,},
                {id:13,name:"Su Su",value:"PPE + Mask",price:180000,},{id:14,name:"Nu Nu",value:"PPE + Mask",price:180000,}
            ]
        }
    }
    w3open= ()=> {
        this.setState({
            display:"block"
        })
      }
    w3close=()=>{
        this.setState({
            display:'none'
        })
    }
    w3SearchOpen=()=>{
        this.setState({
            searchOpen:"none",
            searchClose:"block"
        })
    }
    w3SearchClose=()=>
    {
        this.setState({
            searchClose:"none",
            searchOpen:"block"
        })
    }
    HandleSearch=e=>
    {
        this.setState({search:e.target.value});
    }

render()
{
    let filterName=this.state.DonerName.filter(
        (Names)=>{
            return Names.name.toLowerCase().indexOf(this.state.search.toLowerCase())!==-1;
        }
    );
    return(
        <div style={{height:100+'%'}}>
            <div className="w3-sidebar w3-bar-block w3-border-right" style={{display:this.state.display,id:'mySidebar',zIndex:2}}>
            {/* <button onClick={(e)=>this.w3close(e)} className="w3-bar-item w3-large">Close &times;</button> */}
            <a href="SignIn.html" className="w3-bar-item w3-button w3-green" style={{height:80+'px'}}><i className="fa fa-user-circle" style={{width:30+'px',height:30+'px'}}></i><br/>Sign In</a>
            <Link to="/item" className="w3-bar-item w3-button">Items</Link>
            <Link to="/ppe" className="w3-bar-item w3-button">PPE</Link>
            <Link to="cctv" className="w3-bar-item w3-button">CCTV</Link>
            <Link to="/sesinthu" className="w3-bar-item w3-button">စီစင်သူ</Link>
            {/* <a href="SharePage.html" className="w3-bar-item w3-button">Share app</a>             */}
            <Link to="/sharePage" className="w3-bar-item w3-button">Share App</Link>
            </div>
            <nav className="navbar navbar-expand-sm w3-green fixed-top" style={{zIndex:1}}>
            <div className="w3-bar w3-green">
                <button className="w3-bar-item w3-button w3-green w3-xlarge w3-left" id="btn1" onClick={(e)=>this.w3open(e)}>☰</button>
                <input type="text" placeholder="Search.." onChange={(e)=>this.HandleSearch(e)} className="w3-bar-item" style={{borderRadius:20,width:50+'%',marginTop:7,marginLeft:10+'%',display:this.state.searchClose}} id="searchBtn1"/>
                <button className="w3-bar-item" style={{display:this.state.searchClose,marginTop: 12,backgroundColor:'gray'}} id="searchBtn2" onClick={(e)=>this.w3SearchClose(e)}><i className="fa fa-times"></i></button>
                <button className="w3-bar-item w3-button w3-green w3-xlarge w3-right" id="searchBtn3" onClick={(e)=>this.w3SearchOpen(e)} style={{display:this.state.searchOpen}}><i className="fa fa-search"></i></button>
                <div className="w3-container" onClick={()=>this.w3close()}>
                <h1>အလှူရှင်</h1>
                </div>
            </div>
            </nav>

            <div className="w3-container" onClick={()=>this.w3close()}>
                <table className="w3-table w3-bordered" id="changeTable1">
                <tbody>
                    {filterName.map((v,key)=>
                        {
                            let index=v.name.indexOf(' ');
                            return(
                                <tr key={v.id}>
                                    <td>
                                    <Link to={{pathname: `/ahlushin/${v.id}`}}>
                                    
                                    {/* <p className="w3-left">{v.name}<br/>{v.value}<br/>{v.price}</p><img src={v.imgName} className="w3-right" id="imgStyle" alt="picture"></img> */}
                            <p className="w3-left">{v.name}<br/>{v.value}<br/>{v.price}</p><div className="w3-right" 
                            style={{backgroundColor:'#d1f5d9',fontSize:20,textAlign:'center',fontWeight:'bold',color:'green',width:50,height:50,
                            marginTop:10,paddingTop:7,borderLeftStyle:'groove',borderRightStyle:'groove',borderTopStyle:'groove',borderBottomStyle:'groove'}}>
                                {v.name[0]}{v.name[index+1]}</div>
                                    
                                    </Link>
                                    </td>
                            
                                </tr>
                            );
                        })
                    }
                    <tr style={{height:50}}></tr>
                </tbody>
                </table>
            </div>
            <div className="footer">
            <table id="changeTable2">
            <tfoot onClick={()=>this.w3close()}>
            <tr>
            <td><Link to="./ahluShin"><i className="fas fa-donate" style={{color:'black'}}></i><br/><p>အလှူရှင်</p></Link></td>
            <td><Link to="./ahlukhan"><i className="fa fa-medkit" style={{color:'black'}}></i><br/><p>အလှူခံ</p></Link></td>
            <td><Link to="./sayinmyar"><i className="fas fa-newspaper" style={{color:'black'}}></i><br/><p>စာရင်းများ</p></Link></td>
            <td><a href="#"><i className="fas fa-comment" style={{color:'black'}}></i><br/><p>Chat</p></a></td>
            </tr>
            </tfoot>
            
            </table>
            </div>
        </div>    
    );
}
}
export default HomePage

