import React, {Component} from 'react'
import {render} from 'react-dom'

import QrCode from 'react.qrcode.generator'

class Demo extends Component {
  render() {
    return <div>
      <QrCode value='https://reactjs.org'/>
    </div>
  }
}

render(<Demo/>, document.querySelector('#demo'))