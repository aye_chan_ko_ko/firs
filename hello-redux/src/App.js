// import React from 'react';
// import logo from './logo.svg';
// import './App.css';

// function App() {
//   return (
//     <div className="App">
//       <header className="App-header">
//         <img src={logo} className="App-logo" alt="logo" />
//         <p>
//           Edit <code>src/App.js</code> and save to reload.
//         </p>
//         <a
//           className="App-link"
//           href="https://reactjs.org"
//           target="_blank"
//           rel="noopener noreferrer"
//         >
//           Learn React
//         </a>
//       </header>
//     </div>
//   );
// }

// export default App;

import React, {createRef} from 'react';
import {connect} from 'react-redux';

const Item=({name,price})=>(
  <li>{name},${price}</li>
)

const App =props => {
  let nameRef=createRef();
  let priceRef=createRef();
 
  const add=() =>{
    props.add(
      props.items.length+1,
      nameRef.current.value,
      priceRef.current.value
    );
  }
  return(
    <div>
      <ul>
        {props.items.map(i=>(
          <Item
          key={i.id}
          name={i.name}
          price={i.price}/>
        ))}
      </ul>
      <input type="text" ref={nameRef}/><br/>
      <input type="text" ref={priceRef}/><br/>
      <button onClick={add}>Add</button>
    </div>
  )
}
const stateToProps = state => {
  return {
  items: state
  };
  }
  const dispatchToProps = dispatch => {
  return {
  add: (id, name, price) => {
  dispatch({
  type: 'ADD',
  item: { id, name, price }
  });
  }
  }
  }
  const ReduxApp = connect(stateToProps, dispatchToProps)(App);
  export default ReduxApp;