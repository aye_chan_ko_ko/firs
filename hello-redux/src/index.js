

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Qrmain from './Qrmain'
import {createStore} from 'redux';
import {Provider} from 'react-redux';
import QrCode from 'react.qrcode.generator'
import QrCodeInput from './QrCodeInput';
import {BrowserRouter,
  Switch,
  Route} from "react-router-dom";
import Card from './Card';
import ComponentToPrint from './ReactOneComponentPrint';
import Example from './ReactOneComponentPrint';



ReactDOM.render(
  <Qrmain />,
  document.getElementById('root')
  // document.querySelector('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();

